import React,{useState,useEffect} from 'react'
import Res from '../image/Stage.jpg'
import Pdf from '../pdf/Resource Materials/Grow Big - CA. Ganesh Balakrsihnan.pdf';
import Pdf2 from "../pdf/Resource Materials/Interplay between Ind AS CARO and Schedule III V4 - CA. P R Ramesh-converted.pdf";
import Pdf3 from "../pdf/Resource Materials/SAIF Zone Presentation.pdf";
import Pdf4 from "../pdf/Resource Materials/SIRC Rashmin FEMA - CA. Rashmin Sanghvi.pdf";
 import VideoPop from "../common/videoPop";
import Bcard from "../image/bcard.jpg";
import V1thum from '../image/rs1.jpeg'
import V2thum from '../image/rs2.jpeg'

import V221thum from '../image/v221.jpeg'
import V222thum from '../image/v222.jpeg'


import Footer from '../common/footer';
import axios from 'axios'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import V3thum from '../image/rs3.jpeg'

// import V2thum from '../image/v102.jpeg'
// import Resourcecenter from './resourcecenter';
export default function Resourcecenter() {
  const [videoPop, setVideoPop] = useState(false);
  const [videoPop2, setVideoPop2] = useState(false);

 





useEffect(async() => {
  
  let response = await axios.post(`https://sirc.virtuallive.in:3002/leaderboard`, {userEmail: localStorage.getItem('userEmail'),zoneView:"resource",userName: localStorage.getItem('userName')})

 let response2 = await axios.post('https://sirc.virtuallive.in:3002/lastlogin',
 {userEmail: localStorage.getItem('userEmail'),
 userName: localStorage.getItem('userName'),
 userMemberid: localStorage.getItem('userMemberId'),
 userPhoneNumber:localStorage.getItem('userPhoneNumber')
})
console.log("response",response,response2)


return () => {

};
}
)

// 
// after 15000
const [dataScore2,setScoreData2]=useState(0)
useEffect(async() => {
  let timer1 = setTimeout(async() => {
    let response = await axios.post('https://sirc.virtuallive.in:3002/timespentrecord',{userEmail: localStorage.getItem('userEmail'),pageid:"resource",time:5})
    
//  alert(8)
  setScoreData2(dataScore2+1)
  }, 5000);
  
  return () => {
   clearTimeout(timer1);
  };
},

[dataScore2]
)


// for last login 
const [lastlogin,setLastlogin]=useState(0)
useEffect(async() => {
  let timer1 = setTimeout(async() => {
    let response = await axios.post('https://sirc.virtuallive.in:3002/lastlogin',
    {userEmail: localStorage.getItem('userEmail'),
    userName: localStorage.getItem('userName'),
    userMemberid: localStorage.getItem('userMemberId'),
    userPhoneNumber:localStorage.getItem('userPhoneNumber')
  })
  
//  alert(8)
setLastlogin(dataScore2+1)
  }, 15000);
  
  return () => {
   clearTimeout(timer1);
  };
},

[lastlogin]
)



    return (
        <div
        style={{
          backgroundImage: `url(${Res})`,
          backgroundSize: "cover",
          height: "100vh",
    
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          position: "relative",
        }}>
               <div
          className="flex items-center   justify-center w-100"
          style={{ position: "absolute" ,top:window.innerHeight >900 ? '90%':window.innerHeight > 700?'90%':'88%' } }
        >
         
          <Footer />
        </div> 
         
           <div>
          <div 
          onClick={()=>setVideoPop2(true)}
            className="tc"
            style={{
              position: "absolute",
             
              top:window.innerHeight >900 ? '54%':window.innerHeight > 700?'56%':'53%',
              left:window.innerHeight >900 ? '43%':window.innerHeight > 700?'45%':'44%',
              height: "10%",
              width: "12%",
            }}
          >
            <img
              className="pointer h3 w3"
              src={require("../image/play.gif").default}
              alt=""
            />
            {/* Click Here for video */}
          </div>




          {/* <div 
      
            className="tc"
            onClick={()=>setVideoPop2(true)}
            style={{
              position: "absolute",
             
              top:window.innerHeight >900 ? '56%':window.innerHeight > 700?'57%':'58%',
              left:window.innerHeight >900 ? '55%':window.innerHeight > 700?'72%':'72%',
              height: "10%",
              width: "12%",
            }}
          >
            <img
              className="pointer h3 w33"
              src={require("../image/play.gif").default}
              alt=""
            />
         
          </div> */}
         
        </div>




        {videoPop2 && (
        <VideoPop
        close={setVideoPop2}
        videoUrl={'https://player.vimeo.com/video/645719637'} img={[
          {imgurl:V221thum,videurl:'https://player.vimeo.com/video/645719637',text:'SIRC OF ICAI DAY1 EVENT'},
          {imgurl:V222thum,videurl:'https://player.vimeo.com/video/647729918',text:'SIRC OF ICAI DAY2 EVENT'},
        ]}  />
        )}



        

        </div>
    )
}
